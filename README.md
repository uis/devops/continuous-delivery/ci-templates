# CI templates

This repository contains a set of templates intended to be included into CI jobs
on GitLab. See the comments at the top of each template for usage information.

There is a "common pipeline" which brings in Auto DevOps and "safe" jobs
suitable for a wide array of projects which can be included in your CI
configuration. For example:

```yml
include:
  - project: 'uis/devops/continuous-delivery/ci-templates'
    file: '/auto-devops/common-pipeline.yml'
    ref: v3.0.0
```

See the [common pipeline definition](./auto-devops/common-pipeline.yml) for more
information.

## MR pipelines

After a [long
discussion](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/issues/54),
we have decided that the default behaviour of the common pipeline is to
_disable_ merge request pipelines. This is because the various GitLab template
jobs for security scanning, dependency scanning, etc require that they run in a
branch pipeline.

**For advanced use cases**, add configuration to [update the workflow
rules](https://docs.gitlab.com/ee/ci/yaml/workflow.html#workflowrules-templates)
at the _end_ of the `include` section of your CI configuration. Note that you
will have to override the `rules:` sections of the various security scanning and
test jobs appropriately.

## Adding Common Pipelines

Newly added common pipeline components should be mindful that they can be
triggered in some repositories by the `multi-target-docker-images` pipeline.
This pipeline is used to create multiple output images from a single repository,
and to do so creates a child pipeline to run the actual build.

If the common pipeline being added should run once only in the parent pipeline
it must be explicitly disabled under [the multi target
fragment](fragments/multi-target-docker-images/multiple-docker-images.gitlab-ci.yml).

## Specific documentation

The following documents describe support for specific languages or repository
types:

* [Python projects](./auto-devops/python.md).

# Automatic Version Management in java libraries

The following documents describe support for semantic versioning in java 
libraries.

* [Java projects](./auto-devops/maven.md).