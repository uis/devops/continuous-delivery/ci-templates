# Python support

The following templates are included as part of the [common
pipeline](./common-pipeline.yml).

* [python-tox.yml](./python-tox.yml)
* [python-check-tags-match-version.yml](./python-check-tags-match-version.yml)
* [python-publish.yml](./python-publish.yml)

The jobs are "safe" in that they are only enabled if files are present in the
repository indicating they are sensible to run. All jobs can be disabled
explicitly by setting `..._DISABLED` variables.

## Summary

The common Python pipeline jobs will:

* Build your Python package as a wheel and tarball if there is a `setup.py` or
  `poetry.lock` file in your repository root.
* Run `tox` if there is a `tox.ini` file in your repository root.
* Check that a commit's tag matches the Python package version if there is a
  `setup.py` or `poetry.lock` file in your repository root.
* Publish your package to GitLab's package registry when a commit is tagged and
  if a package was built.
* Publish your package to the test PyPI instance when a commit is tagged,
  a package was built and the `TEST_PYPI_API_TOKEN` variable is set.
* Publish your package to the main PyPI instance when a commit is tagged,
  a package was built and the `PYPI_API_TOKEN` variable is set.

If you have configured publication to both the test PyPI instance and the main
one, publication to the main instance will not happen until there has been a
successful publication to the test one.

Any of the automatically created jobs above may be disabled by setting a
corresponding `..._DISABLED` variable.

## Duplicate pipelines

The Python jobs will run on both merge request and push pipelines. You should
use workflow rules to select an appropriate strategy in your CI configuration to
avoid duplicate jobs. For example:

```yaml
include:
  - template: Workflows/Branch-Pipelines.gitlab-ci.yml
```

## Running tests

The [tox test runner template](./python-tox.yml) will add a "python:tox" job to
commit and merge requests pipelines in the "test" stage if a `tox.ini` file is
present.

The default behaviour is to run `tox` in the three most recent Python versions.
By default the latest version of `tox` is installed and run. If you need to
specify a particular version, use the `TOX_REQUIREMENT` variable. For example,
setting it to `tox==4.12.0` specifies an exact version of `tox` to use.

The job can be disabled by setting the `PYTHON_TOX_DISABLED` variable.

The `tox` command will be run passing it the contents of the `TOX_OPTS`
variable. As such you can arrange for tox testenvs to run in isolated test jobs

If you need to `pip install` additional requirements, you can pass them in the
`TOX_ADDITIONAL_REQUIREMENTS` variable.

If you want code coverage and test reports to be uploaded, arrange for them to
be placed in the following locations:

* Code-coverage: `$TOXINI_ARTEFACT_DIR/py3/coverage.xml`
* Test run results: `$TOXINI_ARTEFACT_DIR/py3/junit.xml`

Any other files present in `$TOXINI_ARTEFACT_DIR` are uploaded as artefacts.

### Customisation

#### Isolating tox testenvs

You can arrange for tox testenvs to run in isolated test jobs by extending the
"python:tox" job.

For example, this configuration will run the `py3` toxenv in the last three
supported Python versions and the `flake8` and `black` toxenvs in the most
recent version.

```yaml
include:
  - project: 'uis/devops/continuous-delivery/ci-templates'
    file: '/auto-devops/common-pipeline.yml'

python:tox:
  parallel:
    matrix:
      # Tests against a standard set of Python versions by setting PYTHON_VERSION.
      - PYTHON_VERSION: !reference [.python:versions]

      # Check formatting with the flake8 and black toxenvs
      - TOX_ENV: flake8
      - TOX_ENV: black
  variables:
    TOX_ENV: py3
    TOX_OPTS: -e $TOX_ENV
```

A corresponding minimal `tox.ini` file which lints using black and flake8, runs
tests using `pytest`, and reports code coverage and test results back to GitLab
looks like the following:

```ini
[tox]
envlist=flake8,black,py3
skipsdist=True

[_vars]
build_root={env:TOXINI_ARTEFACT_DIR:{toxinidir}/build}

[testenv]
deps=
    .
    coverage
    pytest
    pytest-cov
commands=
    pytest --doctest-modules --cov={toxinidir} --junitxml={[_vars]build_root}/{envname}/junit.xml
    coverage html --directory {[_vars]build_root}/{envname}/htmlcov/
    coverage xml -o {[_vars]build_root}/{envname}/coverage.xml

[testenv:py3]
basepython=python3

[testenv:flake8]
basepython=python3
deps=
    flake8==6.0.0
commands=
    flake8 --version
    flake8 --tee --output-file={[_vars]build_root}/{envname}/report.txt .

[testenv:black]
basepython=python3
deps=
    black==23.3.0
commands=
    black --version
    black --check .
```

#### Extending the `before_script`

Sometimes additional dependencies may need to be installed into the gitlab
runner's container in order to run the tests. It is recommended in this case to
overwrite the `before_script` for the `python:tox` job and use a reference to
ensure the template `before_script` is preserved:

```yaml
include:
  - project: 'uis/devops/continuous-delivery/ci-templates'
    file: '/auto-devops/common-pipeline.yml'

python:tox:
  before_script:
    # This runs the 'parent' before_script from the template job, important to
    # include this so that any specified TOX_ADDITIONAL_REQUIREMENTS are still
    # installed, and any other generic required setup is done.
    - !reference [".python:tox", "before_script"]
    # This is our new extension to the before_script:
    - apk add pkgconf
```

The example above would install the alpine linux `pkgconf` package into the
gitlab runner, and therefore make it available for use when installing the test
dependencies.

## Publishing packages

The [publish template](./python-publish.yml) supports building packages from
your code and publishing them to GitLab's package registry and, optionally, the
test and production PyPI instances.

Packages are *built* for push and merge request pipelines as part of the "build"
stage. Package building can be disabled by setting the
`PYTHON_BUILD_DIST_DISABLED` variable.

There are specialised build jobs named "python:build-dist-poetry" and
"python:build-dist-setuptools" for poetry and setuptools-based packaging. Jobs
are enabled automatically based on the presence of `poetry.lock` or `setup.py`
files in the repository. If necessary the build jobs can be selectively disabled
by setting the `PYTHON_BUILD_DIST_POETRY_DISABLED` or
`PYTHON_BUILD_DIST_SETUPTOOLS_DISABLED` variables.

If you push a new tag, packages will be built and published.

If you have also included the [check tags
template](./python-check-tags-match-version.yml), a job named
"python:check-tag-poetry" or "python:check-tag-setuptools" will check that the
commit tag matches the Python package version. You can disable all tag checks by
setting the `PYTHON_CHECK_TAG_DISABLED` variable. If necessary the check jobs
can be selectively disabled by setting the `PYTHON_BUILD_DIST_POETRY_DISABLED`
or `PYTHON_BUILD_DIST_SETUPTOOLS_DISABLED` variables.

The "python:publish-to-gitlab" job runs in the "production" stage for new tags
and publishes your package to GitLab's built in package registry if there is a
`setup.py` or `pyproject.toml` file present in your repository. It can be
disabled by setting the `PYTHON_PUBLISH_TO_GITLAB_DISABLED` variable.

For publishing to PyPI, API tokens need to be created and arranged to be
present in the `TEST_PYPI_API_TOKEN` and `PYPI_API_TOKEN` variables
respectively.

The "python:publish-to-test-pypi" and "python:publish-to-pypi" jobs run like the
"python:publish-to-gitlab" job except that they require the
`TEST_PYPI_API_TOKEN` and `PYPI_API_TOKEN` variables be set respectively. They
can be selectively disabled by setting the
`PYTHON_PUBLISH_TO_TEST_PYPI_DISABLED` and `PYTHON_PUBLISH_TO_PYPI_DISABLED`
variables.

You can extend or override the publish jobs. A template publish job is available
named ".python:publish". The following variables must be set:

* `TWINE_REPOSITORY_URL` - the repository to upload packages to.
* `TWINE_USERNAME` - the username to use to authenticate to the repository.
* `TWINE_PASSWORD` - the password to use to authenticate to the repository.

These publication jobs intentionally do not have a great deal of configuration
available. Complicated packaging should be performed in specialised jobs.
