# Automatic Version Management in maven.gitlab-ci.yml

This pipeline script includes an intelligent **Automatic Version Management** system that ensures the Maven artifact's version is incremented appropriately based on semantic versioning rules. It analyzes commit messages and calculates the next version number for both **snapshot** and **release** builds.

---

## How It Works

### 1. Current Version Detection
- The script queries the GitLab Maven repository to identify the latest published version.
- Filters versions into **snapshot** or **release** based on the current branch.

### 2. Semantic Versioning Rules
- Versions follow the `major.minor.patch` format.
- **Snapshot builds**: Version ends with `-SNAPSHOT`.
- **Release builds**: A clean semantic version (e.g., `1.2.3`).

### 3. Commit Message Parsing
The script analyzes Git commit messages since the last version to determine the type of change:

- `BREAKING CHANGE`: Increments the **major** version.
- `feat:`: Increments the **minor** version.
- `fix:`: Increments the **patch** version.

#### Example:
- Current version: `1.2.3`
- Commit messages:
  - `feat: Add a new feature` → Next version: `1.3.0`
  - `fix: Resolve an issue` → Next version: `1.2.4`
  - `BREAKING CHANGE: Major overhaul` → Next version: `2.0.0`

### 4. Version Adjustment
- Extracts the `major`, `minor`, and `patch` components from the latest version.
- Adjusts the version components based on the commit analysis.

### 5. Snapshot vs. Release
- If the current branch is the default branch (e.g., `main` or `master`), a release version is created.
- For all other branches, a snapshot version is used (e.g., `1.3.0-SNAPSHOT`).

---

## Benefits

- **Automated**: No manual intervention is required to manage version numbers.
- **Consistent**: Ensures all artifacts follow semantic versioning standards.
- **Branch-Specific Behavior**: Supports both snapshot and release workflows.

This automatic versioning system simplifies Maven artifact management and ensures consistency across your deployment process.
