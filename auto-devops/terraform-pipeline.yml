# terraform-pipeline.yml defines the UIS DevOps workflow for deploying product Terraform configurations.
#
# This template is intended to be "include"-d in .gitlab-ci.yml configurations. It uses the base jobs defined in
# terraform-pipeline-base.yml to build a drop in deployment pipeline for our standard GCP products.
#
# To use this template, make sure it's the first template to be "include"-d.
#
# include:
#   - project: 'uis/devops/continuous-delivery/ci-templates'
#     file: '/auto-devops/terraform-pipeline.yml'
#     ref: vx.x.x
#
# Variables
#
# The following variables are available to configure this pipeline's behavior.
#
#   GKE_RUNNER_TAG - Use this to specify the required tag for the product's runner (see
#     https://gitlab.developers.cam.ac.uk/uis/devops/devhub/gitlab-runner-infrastructure)
#
#   TERRAFORM_DEPLOY_IMAGE - The image to use in the CI jobs. Defaults to
#     registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/logan-terraform
#
#   TERRAFORM_DEPLOY_VERSION - The version of the TERRAFORM_DEPLOY_IMAGE to use.
#
# Tests/linting
#
# This template includes the terraform-lint.yml template which adds multiple testing and linting jobs to the pipeline.
# For more information on how these jobs work and how to disable them if required see the terraform-lint.yml file.
#
# Default workflow
#
# The default workflow is that as soon as a commit is made or a merge request is opened the test stage jobs start
# running. For merge requests, the various terraform test jobs will run for both the original commit and the post-merge
# commit and a terraform plan job will run for each environment. Once a merge request is merged to the default branch
# the plan jobs for all environments run again. Then, the staging environment apply job runs automatically (staging is
# therefore always an accurate representation of the default branch). All other environments require a manual trigger in
# the pipeline UI to start their apply jobs.
#
# Disabling jobs
#
# Following the AutoDevOps model, if the following variables are defined, the associated jobs will be disabled:
#
# - TERRAFORM_PLAN_DEVELOPMENT_DISABLED
# - TERRAFORM_APPLY_DEVELOPMENT_DISABLED
# - TERRAFORM_PLAN_STAGING_DISABLED
# - TERRAFORM_APPLY_STAGING_DISABLED
# - TERRAFORM_PLAN_PRODUCTION_DISABLED
# - TERRAFORM_APPLY_PRODUCTION_DISABLED
#
# Extending this pipeline
#
# If you need to add terraform jobs for any workspaces beyond "development", "staging" and "production", you can copy
# an existing environment's terraform-{plan,apply}-... jobs, changing the references to the existing workspace name.

include:
  - local: '/terraform-pipeline-base.yml'
  - local: '/terraform-lint.yml'

# The following jobs make up the default workflow for our standard three-environment products. For products with
# additional environments, these jobs can be copied as require in the product's infrastructure repository directly.
terraform-plan-development:
  extends: .terraform-plan
  stage: development
  rules:
    - if: $TERRAFORM_PLAN_DEVELOPMENT_DISABLED
      when: never
    - !reference [.terraform-plan, rules]
  variables:
    DEPLOYMENT_ENVIRONMENT: development

terraform-apply-development:
  extends: .terraform-apply
  stage: development
  rules:
    - if: $TERRAFORM_APPLY_DEVELOPMENT_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_TAG
      when: manual
      # This is currently the only way to allow the pipeline to succeed if the optional manual development apply job was
      # not triggered. See the following issue for context - https://gitlab.com/gitlab-org/gitlab/-/issues/249524.
      allow_failure: true
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "push"
      when: manual
      allow_failure: true
    - when: never
  variables:
    DEPLOYMENT_ENVIRONMENT: development
  needs:
    - terraform-plan-development

terraform-plan-staging:
  extends: .terraform-plan
  stage: staging
  rules:
    - if: $TERRAFORM_PLAN_STAGING_DISABLED
      when: never
    - !reference [.terraform-plan, rules]
  variables:
    DEPLOYMENT_ENVIRONMENT: staging

terraform-apply-staging:
  extends: .terraform-apply
  stage: staging
  rules:
    - if: $TERRAFORM_APPLY_STAGING_DISABLED
      when: never
    - !reference [.terraform-apply, rules]
  variables:
    DEPLOYMENT_ENVIRONMENT: staging
  needs:
    - terraform-plan-staging

terraform-plan-production:
  extends: .terraform-plan
  stage: production
  rules:
    - if: $TERRAFORM_PLAN_PRODUCTION_DISABLED
      when: never
    - !reference [.terraform-plan, rules]
  variables:
    DEPLOYMENT_ENVIRONMENT: production

terraform-apply-production:
  extends: .terraform-apply
  stage: production
  rules:
    - if: $TERRAFORM_APPLY_PRODUCTION_DISABLED
      when: never
    - !reference [.terraform-apply, rules]
  variables:
    DEPLOYMENT_ENVIRONMENT: production
  needs:
    - job: terraform-apply-staging
      optional: true
    - terraform-plan-production
