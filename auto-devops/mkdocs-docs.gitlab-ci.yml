# Template for building project documentation via mkdocs and publishing via GitLab pages.
#
# Set MKDOCS_DISABLED to disable all mkdocs-related jobs.
# Set MKDOCS_TEST_DISABLED to disable test building of documentation.
# Set MKDOCS_PAGES_DISABLED to disable publishing documentation via GitLab pages.
#
# If a pyproject.toml file is present in the root then dependencies are installed via poetry. Use MKDOCS_POETRY_GROUP to
# specify a group of dependencies to install, otherwise all dependencies are installed.
#
# Otherwise the requirements are installed from $MKDOCS_REQUIREMENTS_FILE which defaults to "requirements.txt".
#
# If there is no requirements file present, then the CI job will fail.
include:
  - local: "/python-common.yml"

variables:
  MKDOCS_REQUIREMENTS_FILE: requirements.txt

# Basic CI to install and run mkdocs build in strict mode with verbose output. We upload the built documentation as an
# artifact so that it can be browsed from MRs or copied to a publish job as necessary.
.mkdocs:
  image: python:${PYTHON_VERSION}-slim
  before_script:
    - |
      set -e
      if [ -f pyproject.toml ] && [ -z "$MKDOCS_DISABLE_POETRY" ]; then
        echo -e "\e[0Ksection_start:`date +%s`:install_environment[collapsed=true]\r\e[0KInstalling documentation dependencies via poetry"
        pip install poetry==${PYTHON_POETRY_VERSION}
        poetry self add poetry-plugin-export
        REQUIREMENTS=$(mktemp -t requirements.XXXXXXXX.txt)
        if [ -z "$MKDOCS_POETRY_GROUP" ]; then
          poetry export -f requirements.txt -o $REQUIREMENTS
        else
          poetry export -f requirements.txt -o $REQUIREMENTS "--only=$MKDOCS_POETRY_GROUP"
        fi
        pip install -r $REQUIREMENTS
        echo -e "\e[0Ksection_end:`date +%s`:install_environment\r\e[0K"
      elif [ -f "${MKDOCS_REQUIREMENTS_FILE}" ]; then
        echo -e "\e[0Ksection_start:`date +%s`:install_environment[collapsed=true]\r\e[0KInstalling documentation dependencies from ${MKDOCS_REQUIREMENTS_FILE}"
        pip install -r $MKDOCS_REQUIREMENTS_FILE
        echo -e "\e[0Ksection_end:`date +%s`:install_environment\r\e[0K"
      else
        echo "No requirements file for mkdocs found. Use poetry or set MKDOCS_REQUIREMENTS_FILE to point to a requirements.txt file to install"
        exit 1
      fi
  needs: []
  script:
    - mkdocs build --strict --verbose --site-dir public --no-directory-urls
  variables:
    # We use --no-directory-urls so that the documentation is browsable from the
    # GitLab artefact browser.
    MKDOCS_ARGS: --no-directory-urls
  artifacts:
    paths:
      - public/

mkdocs:test:
  extends: .mkdocs
  stage: test
  artifacts:
    expose_as: "Browse the documentation for this MR by following links to public and index html"
  rules:
    - if: $MKDOCS_DISABLED
      when: never
    - if: $MKDOCS_TEST_DISABLED
      when: never
    - exists:
        - mkdocs.yml

# For commits to the main branch we additionally build the documentation in a form suitable for GitLab pages.
pages:
  extends: .mkdocs
  stage: build
  variables:
    MKDOCS_ARGS: --directory-urls
  rules:
    - if: $MKDOCS_DISABLED
      when: never
    - if: $MKDOCS_PAGES_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      when: never
    - exists:
        - mkdocs.yml
